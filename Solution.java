import java.util.Arrays;

class Solution {
    public int missingNumber(int[] nums) {
       /*Arrays.sort(nums);
        int i;
        for (i = 0; i < nums.length; i++) {
            if (nums[i] != i) {
                return i;
            }
        }
        return i;*/

        int expected = (1 + nums.length)* nums.length/2;
        int actual = 0;
        for (int num : nums) {
            actual+=num;
        }
        return expected-actual;
    }
}
